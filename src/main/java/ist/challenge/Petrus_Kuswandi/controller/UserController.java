package ist.challenge.Petrus_Kuswandi.controller;

import ist.challenge.Petrus_Kuswandi.dto.UserRequest;
import ist.challenge.Petrus_Kuswandi.models.User;
import ist.challenge.Petrus_Kuswandi.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/users")
public class UserController {
    @Autowired
    private UserService userService;
    @PostMapping("/register")
    public ResponseEntity<String> register(@RequestBody UserRequest userRequest) {
        return userService.registerUser(userRequest);
    }
    @PostMapping("/login")
    public ResponseEntity<String> login(@RequestBody UserRequest userRequest) {
        return userService.loginUser(userRequest);
    }
    @GetMapping("/")
    public List<User> getAllUsers(){
        return userService.getAllUsers();
    }

    @PutMapping("/edit/{id}")
    public ResponseEntity<String> edit(@RequestBody User user){
        return userService.editUser(user);
    }

}
