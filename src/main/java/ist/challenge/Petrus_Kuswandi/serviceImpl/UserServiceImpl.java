package ist.challenge.Petrus_Kuswandi.serviceImpl;

import io.micrometer.common.util.StringUtils;
import ist.challenge.Petrus_Kuswandi.dto.UserRequest;
import ist.challenge.Petrus_Kuswandi.models.User;
import ist.challenge.Petrus_Kuswandi.repository.UserRepository;
import ist.challenge.Petrus_Kuswandi.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
@Service
public class UserServiceImpl implements UserService {
    @Autowired
    private UserRepository userRepository;

    public ResponseEntity<String> registerUser(UserRequest userRequest) {
        Optional<User> existingUser = userRepository.findByUsername(userRequest.getUsername());
        if (existingUser.isPresent()) {
            return ResponseEntity.status(HttpStatus.CONFLICT).body("Username sudah terpakai");
        }

        if (userRequest.getUsername().length() >= 25 || userRequest.getPassword().length() >= 25) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("Username atau Password tidak boleh lebih dari 25 karakter");
        }

        User newUser = new User();
        newUser.setUsername(userRequest.getUsername());
        newUser.setPassword(userRequest.getPassword());
        userRepository.save(newUser);

        return ResponseEntity.status(HttpStatus.CREATED).body("Registrasi berhasil");
    }

    public ResponseEntity<String> loginUser(UserRequest userRequest){
        if (StringUtils.isEmpty(userRequest.getUsername()) || StringUtils.isEmpty(userRequest.getPassword())) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("Username dan / atau password kosong");
        }

        Optional<User> user = userRepository.findByUsername(userRequest.getUsername());
        if (!user.isPresent() || !user.get().getPassword().equals(userRequest.getPassword())) {
            return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body("Username atau password salah");
        }

        if (userRequest.getUsername().length() >= 25 || userRequest.getPassword().length() >= 25) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("Username atau Password tidak boleh lebih dari 25 karakter");
        }

        return ResponseEntity.ok("Sukses Login");
    }

    public List<User> getAllUsers(){
        return userRepository.findAll();
    }

    public ResponseEntity<String> editUser(User user) {
        Optional<User> userOptional = userRepository.findById(user.getId());
        if (!userOptional.isPresent()){
            return ResponseEntity.notFound().build();
        }
        User user1 = userOptional.get();

        if (!user1.getUsername().equals(user.getUsername()) && userRepository.findByUsername(user.getUsername()).isPresent()){
            return ResponseEntity.status(HttpStatus.CONFLICT).body("Username sudah terpakai");
        }

        if (user1.getPassword().equals(user.getPassword())){
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("Password tidak boleh sama dengan password sebelumnya");
        }

        if (user.getUsername().length() >= 25 || user.getPassword().length() >= 25) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("Username atau Password tidak boleh lebih dari 25 karakter");
        }

        user1.setUsername(user.getUsername());
        user1.setPassword(user.getPassword());
        userRepository.save(user1);

        return ResponseEntity.ok("Sukses Edit User");
    }
}
