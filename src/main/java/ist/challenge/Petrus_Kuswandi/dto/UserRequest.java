package ist.challenge.Petrus_Kuswandi.dto;

import jakarta.validation.constraints.Size;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class UserRequest {

    @Size(max = 25, message = "Username tidak boleh lebih dari 25 karakter")
    private String username;

    @Size(max = 25, message = "Password tidak boleh lebih dari 25 karakter")
    private String password;
}
