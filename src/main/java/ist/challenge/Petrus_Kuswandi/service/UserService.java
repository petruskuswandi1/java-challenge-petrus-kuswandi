package ist.challenge.Petrus_Kuswandi.service;

import ist.challenge.Petrus_Kuswandi.dto.UserRequest;
import ist.challenge.Petrus_Kuswandi.models.User;
import org.springframework.http.ResponseEntity;

import java.util.List;

public interface UserService {

    ResponseEntity<String> registerUser(UserRequest userRequest);

    ResponseEntity<String> loginUser(UserRequest userRequest);

    List<User> getAllUsers();

    ResponseEntity<String> editUser(User user);
}
